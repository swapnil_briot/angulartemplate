import { Component, Injectable, ViewChild, OnDestroy, OnInit, Inject, ElementRef } from '@angular/core';
declare var $;
import { Router } from '@angular/router';
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material/dialog';
import { FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
// import { ToastrService } from 'ngx-toastr';
// import { NgxSpinnerService } from 'ngx-spinner';
// import { Http, Headers, Response,RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { pipe } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-auth-signin',
  templateUrl: './auth-signin.component.html',
  styleUrls: ['./auth-signin.component.scss']
})
export class AuthSigninComponent implements OnInit, OnDestroy {

  username: string;
  password: string;
  userName: string;
  Password: string;

  errorMessage: string;
  successMessage: string;
  showSpinner: boolean = false;
  loginDetails: any;
  hide = true;
  urlData: any;
  private _jsonURL = 'assets/url.json';
  loginForm: FormGroup;

  @ViewChild(FormGroupDirective,{static:true}) formDirective: FormGroupDirective;
  @ViewChild("name", { static: true }) nameField: ElementRef;
  constructor(private http: HttpClient,
    private router: Router) {
    this.getJSON().subscribe(data =>
      this.urlData = data["url"]
    );
  }
  public getJSON() {
    return this.http.get(this._jsonURL)
      .pipe(map((response: Response) => {
        let responsejson = response;
        if (responsejson) {
        }
        return responsejson;
      }));
  }
  
  ngOnInit() {
    this.initLoginForm();
  }
  ngOnDestroy(): void {
    $('body').removeClass('hold-transition login-page');
  }

  initLoginForm() {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })
  }
  login() {
    var json = {
      username: this.username,
      password: this.password
    }
    console.log("JSON DATA", json)
    let header = new HttpHeaders({
      'Content-Type': 'application/json'      
    });
    console.log("URL Data", this.urlData);
    this.http.post(this.urlData + `/users/sign_in`, json, { headers: header }).subscribe(
      data => {        
        if (data) {          
          sessionStorage.setItem('currentUser', JSON.stringify(data));
          this.router.navigate(['dashboard/default']);
        }
      },
      error => {
        console.log(error);
        // this.toastr.error(error);
        // Swal.fire('Oops...', 'Something went wrong!', 'error');
        this.resetInput();
      }
    );
  }

  forgotPassword(): void {
    // this.toastr.info("Contact your admin to reset your password");
    // Swal.fire('Oops...', 'Contact your admin to reset your password!', 'error')
    this.resetInput();
  }

  resetInput() {
    this.username = "";
    this.password = "";
  }

}
