import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './theme/layout/admin/admin.component';
import {AuthSigninComponent} from '../app/demo/pages/authentication/auth-signin/auth-signin.component';
import {AuthComponent} from './theme/layout/auth/auth.component';
import {DashDefaultComponent} from '../app/demo/dashboard/dash-default/dash-default.component';

const routes: Routes = [
{path: '', redirectTo: 'login', pathMatch: 'full' },
// {
//   path: 'dashboard/default',
//   component:AdminComponent,
//   loadChildren: () => import('./demo/dashboard/dashboard.module').then(module => module.DashboardModule)
// },
  {
    path: 'dashboard/default',
    component: AdminComponent,
    loadChildren: () => import('./demo/dashboard/dashboard.module').then(module => module.DashboardModule),
    // children: [
    //   {
    //     path:'',
    //     component:AdminComponent
    //   },
    //   {
    //     path: 'dashboard/default',
    //     loadChildren: () => import('./demo/dashboard/dashboard.module').then(module => module.DashboardModule)
    //   },     
    //   // {
    //   //   path: 'dashboard',
    //   //   loadChildren: () => import('./demo/dashboard/dashboard.module').then(module => module.DashboardModule)
    //   // },
    //   {
    //     path: 'layout',
    //     loadChildren: () => import('./demo/pages/layout/layout.module').then(module => module.LayoutModule)
    //   },
    //   {
    //     path: 'basic',
    //     loadChildren: () => import('./demo/ui-elements/ui-basic/ui-basic.module').then(module => module.UiBasicModule)
    //   },
    //   {
    //     path: 'forms',
    //     loadChildren: () => import('./demo/pages/form-elements/form-elements.module').then(module => module.FormElementsModule)
    //   },
    //   {
    //     path: 'tbl-bootstrap',
    //     loadChildren: () => import('./demo/pages/tables/tbl-bootstrap/tbl-bootstrap.module').then(module => module.TblBootstrapModule)
    //   },
    //   {
    //     path: 'charts',
    //     loadChildren: () => import('./demo/pages/core-chart/core-chart.module').then(module => module.CoreChartModule)
    //   },
    //   {
    //     path: 'sample-page',
    //     loadChildren: () => import('./demo/pages/sample-page/sample-page.module').then(module => module.SamplePageModule)
    //   }
    // ]
  },
  {
    path: 'login',
    component: AuthSigninComponent,
    children: [
      {
        path: 'auth',
        loadChildren: () => import('./demo/pages/authentication/authentication.module').then(module => module.AuthenticationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
